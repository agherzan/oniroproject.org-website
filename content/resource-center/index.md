---
title: "Resource Center"
date: 2021-10-06T10:00:00-04:00
footer_class: "footer-darker"
hide_sidebar: true
layout: "single"
---

The process to create the Oniro Working Group and Top Level Project started on October 26th.
The Oniro project will benefit from a substantial initial contribution resulting from more than a year of intensive work by the founding members of the Working Group.
You can access these resources now, currently hosted at Eclipse:
* [Get the Code](https://gitlab.eclipse.org/eclipse/oniro-core)
* [Documentation](http://docs.oniroproject.org)
* [Talk to us](https://docs.oniroproject.org/en/latest/community-chat-platform.html) at [Libera chat](https://web.libera.chat/#oniroproject) - channel #oniroproject

To learn more about the Oniro project at the Eclipse Foundation, check out the:
* [Oniro Top Level Project](https://projects.eclipse.org/projects/oniro)
* [Top Level Project Charter](https://projects.eclipse.org/projects/oniro/charter)
